---
name: Homework Assignment
about: Use this template to submit the homework
title: "[HW] <NAME>"
labels: homework, wait for review
assignees: HadesArchitect, SonicDMG, RyanWelford

---

**Name:** REPLACE_ME
**Email:** REPLACE_ME
**Linkedin Profile:** <LINK>

Attach the homework screenshot below: Remember, you only need the last screen of the step you have completed, step IV or V
-----------------------------------------

<SCREENSHOT>
